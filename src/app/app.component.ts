import { Component, OnDestroy, OnInit } from '@angular/core';
import { ApolloQueryResult } from '@apollo/client/core';
import { Apollo, gql } from 'apollo-angular';
import { Subscription } from 'rxjs';

const LOGIN_QUERY = gql`
  query Login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
      faculty {
        name
        email
        department
      }
    }
  }
`;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'client';

  loading: boolean = true;
  data: string = '';
  login_sub: Subscription;

  constructor(private apollo: Apollo) {
    this.login_sub = Subscription.EMPTY;
  }

  ngOnInit(): void {
    this.login_sub = this.apollo
      .watchQuery<any>({
        query: LOGIN_QUERY,
        variables: {
          email: 'test@mail.com',
          password: 'password',
        },
      })
      .valueChanges.subscribe((result: ApolloQueryResult<any>) => {
        this.data = JSON.stringify(result.data, null, 4);
        this.loading = false;
      });
  }
  ngOnDestroy(): void {
    this.login_sub.unsubscribe();
  }
}
